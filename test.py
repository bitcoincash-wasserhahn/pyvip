import pyvip
import unittest
import os
import time

from pyvip import public as vippublic
from datetime import datetime


class PyVipPrivateTest(unittest.TestCase):
    def setUp(self):
        self.API_KEY = os.getenv('VIP_API_KEY')
        self.API_SECRET = os.getenv('VIP_API_SECRET')
        self.vip = pyvip.Client(self.API_KEY, self.API_SECRET)
        self.vip.trust_env(True)

    def test_get_info(self):
        self.assertTrue(self.vip.get_info()['success'] == 1)

    def test_trans_history(self):
        self.assertTrue(self.vip.trans_history()['success'] == 1)

    def test_trade_history(self):
        self.assertTrue(self.vip.trade_history()['success'] == 1)

    def test_trade_history_with_pair(self):
        self.assertTrue(self.vip.trade_history('btc_idr')['success'] == 1)

    def test_trade_history_with_all_kwargs(self):
        self.assertTrue(self.vip.trade_history('str_btc', count=10, from_id=1, end_id=1000000, order='asc',
                                               since=time.mktime(datetime(2011, 1, 1).timetuple()),
                                               end=time.mktime(datetime.now().timetuple())
                                               )['success'] == 1)

    def test_trade_history_with_wrong_pair(self):
        self.assertTrue(self.vip.trade_history('anu_anu')['success'] == 0)

    def test_trade_history_with_wrong_kwargs(self):
        with self.assertRaises(KeyError):
            self.vip.trade_history(anu='anu')

    def test_open_orders(self):
        self.assertTrue(self.vip.open_orders()['success'] == 1)

    def test_open_orders_with_pair(self):
        self.assertTrue(self.vip.open_orders('doge_btc')['success'] == 1)

    def test_open_orders_with_wrong_pair(self):
        self.assertTrue(self.vip.open_orders('anu_btc')['success'] == 0)

    def test_order_history(self):
        self.assertTrue(self.vip.order_history()['success'] == 1)

    def test_order_history_with_kwargs(self):
        self.assertTrue(self.vip.order_history(pair='str_idr', order_count=10, order_from=0)['success'] == 1)

    def test_order_history_with_bad_value(self):
        self.assertFalse(self.vip.order_history(pair='str_idr', order_count=-1, order_from=-10000)['return']['orders'])

    # test sell coin with imposible price
    def test_trade_getorder_cancelorder(self):
        pair = 'str_btc'
        trx_type = 'sell'
        trx = self.vip.trade(pair=pair, trx_type=trx_type, price=1000000, amount=100)
        order_id = trx['return']['order_id']
        self.assertTrue(trx['success'] == 1 and order_id)
        self.assertTrue(self.vip.get_order(order_id=order_id, pair=pair)['success'] == 1)
        self.assertTrue(self.vip.cancel_order(order_id=order_id, pair=pair, trx_type=trx_type)['success'] == 1)

class PyVipPublicTest(unittest.TestCase):
    start_time = int(time.mktime(datetime(2017, 12, 31).timetuple()))
    end_time = int(time.mktime(datetime.now().timetuple()))

    def test_get_ticker(self):
        self.assertTrue(vippublic.ticker()['ticker'])

    def test_get_ticker_with_wrong_symbol(self):
        self.assertTrue(vippublic.ticker('anu_anu')['error'])

    def test_get_trades(self):
        trades_data = [key for key in vippublic.trades()[0]]
        trades_key = ['date', 'price', 'amount', 'tid', 'type']

        trades_data.sort()
        trades_key.sort()

        self.assertListEqual(trades_key, trades_data)

    def test_get_trades_with_wrong_symbol(self):
        self.assertTrue(vippublic.trades('anu_anu')['error'])

    def test_get_depth(self):
        depth_data = vippublic.depth()
        self.assertTrue(depth_data['buy'] and depth_data['sell'])

    def test_get_depth_with_wrong_symbol(self):
        self.assertTrue(vippublic.depth('anu_anu')['error'])

    def test_get_historical(self):
        self.assertTrue(vippublic.historical(self.start_time, self.end_time, '1', 'etc_idr')['s'] == 'ok')

    def test_get_historical_with_wrong_time(self):
        self.assertTrue(vippublic.historical(start_time=self.end_time, end_time=self.start_time,
                                             resolution='1', symbol='xlm_idr')['s'] == 'no_data')

    def test_get_historical_with_wrong_pair(self):
        historical_data = vippublic.historical(self.start_time, self.end_time, '1', 'wkwkland')
        self.assertTrue(historical_data['s'] == 'error' and
                        historical_data['errmsg'])

    def test_get_historical_with_wrong_resolution(self):
        with self.assertRaises(KeyError):
            vippublic.historical(self.start_time, self.end_time, 'M')


if __name__ == '__main__':
    unittest.main()
